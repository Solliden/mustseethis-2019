                var CurrentIndex = 0
                var ArtistArray = ["Drake", "Jeremih", "Chris Brown", "Joyner Lucas", "G-Eazy","Tyga", "Post Malone","Chance The Rapper"];
                var ArtistBornAndFromArray = ["Age 32 - Canada","Age 31 - United States","Age 29 - United States","Age 30 - United States","Age 29 - United States","Age 29 - United States","Age 23 - United States","Age 25 - United States"]
                var Fact1Array = ["Drake is the highest-certified digital singles artist ever in the United States.","At a young age, Jeremih taught himself to play the drums, piano and saxophone by himself."
,"Chris Brown was inspired to make music by Sam Cooke, Stevie Wonder and Michael Jackson.","His initial rapper name was going to be Future Joyner, but he dropped it after another artist","G-Eazy is of Ukrainian descent, but is born in Oakland, California. He also spends most of his time there.","He procured a deal with Young Money Entertainment after his Young On Probation mixtape in 2007 was well received.","White Iverson was written two days before recording, and was never planned to be recorded","Chance is the first artist to win a Grammy based solely on streaming. That is a unique achievement!"]
                var AchievementArray = ["32 Nominations and 8 times winner of the BET awards. That is a lot of nominations!","His single Dont Tell Em with YG became his third top-ten hit on the Billboard Hot 100","Chris Brown became a teen heartthrob, landing R&B and pop hits that included Run It, Kiss Kiss and Forever.","In 2017, Lucas released his fourth mixtape, which was his first on a major label.","Won People's Choice awards in 2017 as a Favourite Hip-Hop artist. Rightfully so!","Tyga won the Much Music Video Award for Much Vibe Hip Hop Video of the year in 2012, with Drake and Lil Wayne.","In 2018, Post has received a Billboard Music Award for Top Rap Song - Rockstar.","Chance the Rapper is a hip-hop artist, producer and a social activist. He produces music without a label."]
                var Fact2Array = ["He is one of the most successful rappers in the history.","Jeremih currently holds a total net worth of $2 million.","He grew up in Tappahannock, Virginia.","Joyner has a son whom he references frequently in his music.","G-Eazy took a big break until 2012 after Lil Wayne and Drake took him on a tour.","Tyga was once robbed of his gold chain with diamonds that later ended up with rapper 40 Glocc.","His real name is Austin Richard and is born in Syracuse, New York.","Chance has donated 1 million dollars of his own money to Chicago's public school system."]
                var WebsiteLinkArray = ["https://drakeofficial.com/","http://www.jeremih.com/","http://undecided.chrisbrownworld.com/","https://www.joynerlucas.com/","https://g-eazy.com/","https://tygatyga.com/","http://www.postmalone.com/","http://chanceraps.com/"]
                var ImageArray = ["Artists/Drake_Resized.png","Artists/Jeremih_Resized.png","Artists/ChrisBrown_Resized.png","Artists/JoynerLucas_Resized.png","Artists/G-Eazy_Resized.png","Artists/Tyga_Resized.png","Artists/PostMalone_Resized.png","Artists/ChanceTheRapper_Resized.png"]
                function UpdateValues(IndexValue)
                {
                    
                    if (CurrentIndex <= 0 && IndexValue == -1)
                     {
                        CurrentIndex = 7;
                    }
                    else if (CurrentIndex >= 7 && IndexValue == 1)
                    {
                        CurrentIndex = 0;
                    }
                    else
                    {
                        CurrentIndex += IndexValue;
                    }
                    document.getElementById("ArtistName").innerHTML = ArtistArray[CurrentIndex];
                    document.getElementById("BornAndFrom").innerHTML = ArtistBornAndFromArray[CurrentIndex];
                    document.getElementById("Fact1").innerHTML = Fact1Array[CurrentIndex];
                    document.getElementById("Achievement").innerHTML = AchievementArray[CurrentIndex];
                    document.getElementById("Fact2").innerHTML = Fact2Array[CurrentIndex];
                    document.getElementById("ArtistButton").href = WebsiteLinkArray[CurrentIndex];
                    document.getElementById("ArtistImage").src = ImageArray[CurrentIndex];

                }

                function Validation()                                    
                { 
                    var FirstName = document.forms["ContactForm"]["FirstName"].value;               
                    var PhoneNumber = document.forms["ContactForm"]["PhoneNumber"].value;    
                    var EmailAddress = document.forms["ContactForm"]["EmailAddress"].value;
                    var EmailAddress2 = document.forms["ContactForm"]["EmailAddress2"].value;
                    var Message =  document.forms["ContactForm"]["Message"].value;  
                   
                    if (FirstName == "")                                  
                    { 
                        window.alert("Please enter your first name."); 
                        return false;
                    } 
                   
                    else if (PhoneNumber == "")                               
                    { 
                        window.alert("Please enter your phone number."); 
                        return false; 
                    } 
                       
                    else if (EmailAddress == "")                                   
                    { 
                        window.alert("Please enter your email address."); 
                        return false; 
                    } 
                   
                    else if (EmailAddress.indexOf("@", 0) < 0)                 
                    { 
                        window.alert("Please enter a valid e-mail address."); 
                        return false; 
                    } 
                   
                    else if (EmailAddress.indexOf(".", 0) < 0)                 
                    { 
                        window.alert("Please enter a valid e-mail address."); 
                        return false; 
                    }
                    else if (EmailAddress2 != EmailAddress)
                    {
                        window.alert("Make sure your email address is the same.")
                        return false;
                    }
                   
                    else if (Message == "")                           
                    { 
                        window.alert("Please enter a message!");  
                        return false; 
                    }

                    else if (Message.length >= 121)
                    {
                        window.alert("Please enter a message within 120 characters.")
                        return false;
                    }
                    else {
                        return true;
                    }
                }